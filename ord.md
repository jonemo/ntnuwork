# andre kjekke ting

| i | 2^i |
| ---- | ---- |
|1| 2 |
|2| 4 |
|3|8|
|4|16|
|5|32|
|6|64|
|7|128|
|8|256|
|9|512|
|10|1024|

1 int = 32 bit

32 bit system = 2^32 byte adresserom

|  | bit|
| ---- | ---- |
|1 bit | 2^0 bits|
|1 byte | 2^3|
|1 kB | 2^13|
|1 mB | 2^23|
|1 GB | 2^33|
|1 TB | 2^43|
| 10n - 7 | 2^(10*n + 3) |

# 1

#### prosess
- et program som kjøres

#### adresserom
- et rom der et program kan bruke av minne

#### fil
- et konsept som OS skaper
- egt en plass på harddisken

#### kernel mode
- modus hvor OS kjøres
- har tilgang til alle komponentene i datamaskinen
- det stygge gjøres her

#### user mode
- det er det interfacet som OS skaper
- det er der programmer som vi laster ned kjøres
- mye lettere å bruke enn kernel mode

#### mode switch/transition
- skifiting mellom kernel og usermode

#### preemptive multitasking
- en harddisk har fått tildelt et visst tidsrom som det får lov til å kjøre på CPUen
- avsluttes eller preemtes etter at tiden er slutt

#### multiprogrammering
- det faktum at flere programmer lastes inn i memory.
- velger mellom hvilket program som skal kjøres.

#### timesharing
- deler dataresurrser mellom flere brukere samtidig.

#### batch jobb/prosess
- en jobb som kjøres i CPU
- uten input fra brukeren

#### interaktiv jobb/prosess
- en jobb som tar input fra brukeren

#### GNU
- operativsystem standard
- prosjekt for programvare
- startet i 1983 av Richard stallan

#### POSIX
- standarden for alle linux og dossytem.

#### bit/Byte
- 1 bit = 1* 0 eller 1
- 1 byte = 8 bit

#### KB/MB/GB/TB/PB/EB
- 1 Kilo byte = 1000 byte
- 1 mb byte 1000 kb
- 1 gb = 1024 mb
- 1 tb = 1000 gb
- 1 petabyte = 1000 tb
- 1 exabyte = 1000 petabyte
- øker alltid med 1000 av den forrige verdien.

#### ms/us/ns
- milisekund = 1/1000 sekund
- microsekund = 1/1000000 sekund = 1/1000 ms
- nanosekund = 1/1000 microsekund

# 2

#### bus
- en kabel der informasjon blir sendt over. mellom ulike enheter i maskinen

#### MMU
- memory management unit
- utdeler minneomeråde(adresserom) til hvert program.

#### kontroller
- en enhet som kontrollerer informasjon langs bussen.
- har ofte en egen prosessor, minne og programvare

#### firmware
- programvare som brukes til å operere på de ulike enhetene i datamaskinen.
- den ligger på kontrolleren.

#### instruksjon
- en instruksjon som forteller cpu hva den skal gjøre

#### instruksjonssettmikroarkitektur
- hver produsent av cpu har sin egen metode å gjøre instrukser på
- generelle enigheter.

#### register
- lagringsplass direkte på cpu som kan lagre data.

#### AX/BX/CX/DX/SP/BP/IP/IR/FLAG/PSW
- ulike former for registre
- Flag viser svaret på sammenligninger
- IP = instruction pointer, samme som program counter. peker på neste instruks
- BP = base pointer. peker alltid øverst på nåværende stackframe.
- SP = stack pointer. peker alltid øverst på stacken.
- PSW = register som viser hvilken tilstand cpuen er i. (context/mode switch)
- IR = instruction register. peker på nåværende instruks
- e(a, b, c)x = generell lagring av informasjon.


#### adresse
- viser til en spesifikk plass i ram.

#### adresseringsmodus
- hvordan arkitetkuren identifiserer operandene for hver instruksjon.

#### interrupt
- arbeidsoppgave som må skje i kernel mode.
- spesifikt oppgaver som kommer uforutsigbart (asynkront) for datamaskinen. kommer som oftest fra input enheter.
- dette er oppgaver som MÅ utføres med engang. cpu dropper alltid det den jobber med.
- forutsigbare interrupts kalles synkrone.
  - exceptions og traps/software interrupt.

#### IRQ
- interrupt request

#### stack/push/pop
- stack er vel egentlig et rom for å lagre data. vo
- som man pusher data inn til
- og popper data ut av

#### context switch
- skifter mellom ulike programmer.

#### text/data/stack
- ulike former for data i en assembly perspektiv
- .data er der globale variabler blir lagret
- .text er selve programmet er lagret. kode delen
- .stack er alt det andre som er viktig for at programmet skal kjøre.

#### objektfil
- filer som inneholder maskin kode.
- det som er det normale resultatet av en kompilator fra c/c++

#### assembly-direktiv
- sier noe om hvilken type data det er som kommer

#### assembly-merke/label
- et merkelapp for en funksjon
- kan hoppes til med jmp
- ":main", "jmp main"

#### byte/word/long/quadword
- ulike ord for størrelse på data.

#### mnemonic
- et synbolsk navn for en spesifikk maskinvare oppgave
- gruppering av masse opkoder. en optkode peker på hvilken maskinvare oppgave som skal utføres

#### klokke-hastighet/takt/periode
- en betegnelse for hvor mye ting en cpu kan gjøre per sekund

#### Hz
- selve tallet angir hvor mange gang en puls slår i sekundet
- 1 hz = 1 slag i minuttet.
- 1 ghz = 1 oppgave i nanosekunet. 1 Giga = 1 milliard i sekundet, ==> nano

#### pipeline
- en lang arbeidslinje av enheter. der hver enhet har hver sin spesifikke oppgave

#### micro-operasjoner
- det at en cpu hele tiden tar en stor oppgave og deler den opp i mye mindre deler.
- divide and conqure.
- 1 del i pipeline utfører 1 type micro operasjon

#### out-of-order utførelse
- en cpu kan gjøre oppgaver i en helt annen rekkefølge den ble gitt i.
- dette er for optimalisering
- også noe i en superskalar prosessor

#### superskalar
- når man har flere enn 1 pipeline vendt inn mot CPUen.
- generelt kan utføre mer enn 1 instruks hver klokkepuls

#### spekulativ eksekvering
- en cpu gjetter hvor en jump/branch assembly instruks skal hoppe hen.
- deretter går den tilbake hvis den gjettet feil.

#### SMT/hyperthreading
- hyperthreading er det at en cpu har dobbelt opp sett med registre.
- dette innebærer at den har mye mer lagringsplass å jobbe med.
- en utvidelse til en superskalar prosessor

#### von Neumann-flaskehalsen
- problemet med at det tar lang til for en cpu å aksessere en plass i RAM
- spesielt når man går til samme plass flere ganger
- løses oftest med en cache imellom cpu og ram

#### spatial/temporal locality
- det at en adresse kan ikke være på samme sted til samme tid i en cache.
  - noe som er ikke ønskelig.
- derfor er det ulike teknikker for dette forklart under.

#### cache line
- en cache er en enhet mellom cpu og ram.
- en cache mellomlagrer informasjon som har blitt hentet fra ram tidligere.
- dette gjør at informasjon kan hentes raskere.
- en cache line er en plass i en slik cache

#### set associative cache
- bruker en del av adressen som skal i cache som en nøkkel til hvor den er lokalisert i cache.
- deretter har man ulike ways innenfor hver plass som kan lagre data. så hvis det blir dobbelt lagring så kan en ny way benyttes.

#### fully associative cache
- alle celler i cache kan ha hvilken som helst adresse.

#### write through/back cache
- writeback cache. skriver tilbake data fra cache til ram etter en gitt tid
- write through cache skriver til ram sammen med at den skriver til cache.

#### cache coherence
- en protokoll for hvordan man behandler cache når man har flere kjerner

#### sylinder/sektor/spor/plate
- en SSD lagringsplass
- moderne lagringstape

#### busy waiting
- en metode å ta input på

#### DMA
- fungerer som en liten maskin mellom cpu og harddisk
- en arbeidsenhet som laster inn X mengde data fra harddisk i memory eller cache.
- direct memory acess.

#### interrupt vektor tabell
- tabell over hvilken andresse interrupt kommer fra og skal sende til.

#### interrupt vektor
- en adresse til koden for den tilhørende interrupt handleren i minne

#### synkrone/asynkrone interrupt
- synkrone interrupt er interrupt som skjer i sammenheng med en jobb i prosessoren. (kommer fra maskinen selv)
  - exceptions: deling på null mattematiske feil
  - traps/software: brukes til systemkall
- asynkrone interrupt er interrupt som skjer helt uavhengig av det som skjer i dataen
  - feks I/O fra mus og tastatur.

#### HW-interrupt
- interrupt fra hardware asynkront.
- knyttet til I/O

#### Exception (synkront)
- når du gjør noe feil matematisk
- dele på null, minne som ikke finnes.

#### SW-interrupt/Trap (synkront)
- interrupt som kommer fra maskinen selv. kommer fra en instruks i prosessoren
- brukes ofte til systemkall

#### BIOS/MBR
- det lille operativ systemet som kjører i hovedkortet.

# 3

#### mainframe
- store datamaskiner

#### embedded systems
- systemer som er mindre, kortlesere og div.

#### soft/hard real-time
- forskjellen mellom hvor kristisk responsiv et operativsystem må være

#### preemptive/non-preemptive
- preemptive kan avslutte prosesser som er midt i cpu
- non-preemtive kjører programmet til den er ferdig

#### klokkeinterrupt
- et interrupt som blir sendt i et preemptive system
- blir sendt hvert x te klokkeslag

#### PCB
- Process Control Block
- en block med all informasjon som trengs om en prosess.

#### prosesstabell
- en tabell med alle prosesser som ligger i scheduler
- en entry her er en PCB

#### prosesshierarki/tre (SØKT)
- det faktum at i linux/unix systemer så er prosesser alltid blitt laget fra en annen prosess
- ps tree i terminal

#### systemkall
- et kall fra userspace til kernel om en oppgave som må utføres.

#### kernel space
- området i minnet der kernel mode kjøres

#### PID
- alle prosesser får en unik pid kode

#### fork
- en prosess kan lage tråder av seg selv. dette er prosesser som utfører småoppgaver
- dette gjøres med fork() igjennom pthread bibloteket i C
- fork() returnerer 0 dersom koden er en thread.

#### WinAPI
- windows har en veldig knotete systemkall system
- derfor har de laget et grensesnitt på toppen som kan fritt brukes.

#### monolittisk
- en kernel som har tilgang til mye mer funksjonalitet enn en microkernel
- dette er for å kunne gjøre ting mye kjappere og slippe å skifte mellom user/kernel mode så ofte.

#### mikrokjerne
- en kjerne der mye funksjonalitet ligger i usermode

#### beskyttelsesringer (SØKT)
- oppdeling av operativsystemet i flere lag.
- dette gjøres etter lagdelingsstrukturen. noe som er kjent

#### type1/type2 hypervisor (SØKT)
- i type1 hypervisor så er de virtuelle operativsystemene installert ved siden av hverandre.
  - alle er under 1 hypervisor inkludert hoved OS
- i type2 hypervisor så er den virtuelle operativsystemet på toppen av en type 2 hypervisor
  - operativ systemet ligger ikke under denne hypervisoren. den ligger nærmest hardware.

# 4

#### prosess eier
- alle prosesser har en bruker som har startet de. dette er da som oftest enten root eller brukernavnet

#### signal
- prosesser kan sende signaler til hverandre hvis de er laget av samme bruker.

#### brukerprosess (SØKT)
- en prosess ikke laget av root. av brukeren. vanlig program

#### service/daemon-prosess (SØKT)
- dette er oppgaver som kjøres uten at brukeren er involvert
- samme konsept som batch jobb, bare at batchjobb er kortvarig. det er ikke dette.

#### prosesstilstander
- en prosess kan ha 3 ulike tilstander.
- ready
  - klar til å bli valgt til å få cpu tid
- running
  - kjører på cpu akkurat nå
- blocked
  - blokkert av et interrupt/scheduler

#### readykø
- kø med alle prosessene som er klare til å få tildelt cpu tid.

#### CPU/IO-bound
- applikasjoner som er CPU bound tar mye kalkulasjoner for å kunne kjøre
  - feks vitenskapelige programmer
- IO-bound er programmer som tar mest tid i usermode og for å få input/output.

#### dispatcher/worker-tråd
- dispatcher er en tråd som fordeler ut arbeid til worker tråder
  - det er trådsentralen for hver prosess
- en worker tråd er en tråd laget av en dispatcher til å gjøre en liten jobb

#### ikke-blokkerende systemkall (SØKT)
- systemkall som kan gjøres uten at det må ventes før den kan kjøres
- getpid og div.
- read er et blokkerende systemkall

#### Pthread
- bibloteket som brukes til å jobbe med tråder i C

#### user/kernel-level tråder (SØKT)
- kernel level tråder har ingen runtime system den svarer til
  - opererer på samme nivå som prosesser. handlet av OS.
- user level er laget av et runtime system.
  - hele prosessen er ødelagt hvis noen gjør en blokkerende kall

#### runtime system
- systemet i dispatcher som deler ut arbeid.

# 5

#### atomisk
- udelelig
- bare en ting kan skje om gangen.
- brukes ofte i race condtions

#### deadlock
- to prosesser venter på hverandre i uendelig tiden

#### starvation
- ikke en prosess får tildelt nokk cpu som den trener
- problem med scheduling

#### Race condition
- et program som har flere tråder gir forskjellig svar hver gangen
- dette er fordi svaret avhenger av hvilken tråd som kommer først fram til å endre en variabel

#### kritisk sektor
- en sektor hvor en prosess gjør noe kritisk feks endre på en variabel
- når en tråd er i en kristisk sektor kan ikke noen andre tråder endre på den variabelen

#### spin lock
- en variabel som gjør at en tråd blir i busy waiting for at det skal bli den sin tur
- en variabel som tilegner hvem sin tur det er for flere prosesser.

#### spin or switch
- problemstillingen om man skal la tråder spinne eller om man skal blokkere en tråd fra å komme på cpu.

#### TSL/XCHG
- to praktiske løsninger for løsningen på race condition

#### priority inversion
- hvis man har en høy og lav prioritet prosesser
- så kan det skape et problem.
- høy prioritets prosessen indirekte blir preemptet fra cpu av en lav prioritetsprosess.
  - den høye skal egt bare kunne bli kastet ut av en annen høy prosess.

#### semafor (up/down)
- semafor er en int som ikke kan gå under 0.
  - hvis den blir under 0, så blir denne tråden sendt i busy waiting
- down = minking
- up = øking av semaforen.

#### sem_wait/sem_post
- sem_wait = down
- sem_post = up

#### mutex (lock/unlock)
- en semafor som kun er lovlig mellom 0 og 1
  - egt en binær semafor men er egt 2 forskjellige ting. liten forskell
- samme bruker som låste må også åpne den.
- ofte brukt til å kontrollere tilgang til kritisk sektor.

#### binær semafor
- samme som en mutex
- bare at en binær semafor kan åpnes å lokkes av alle.

#### betingelsesvariabel
- et alternativ til semafor.
- sender signaler til de ulike prosessene som venter på den samme variabelen
- alle trådene venter på en betingelse.

#### monitor
- compilatoren tar ansvar for behandling av tilgang til kritisk sektor for flere tråder.
- innebygd i høy programmeringsspråk

#### barrier
- man venter på at alle prosessene skal bli ferdig, før programmet fortsetter.

# 6 scheduling

#### long-term/admission scheduler
- øverste nivå av scheduler.
- bestemmer om en prosess får lov til å komme inn i readykøen eller ikke.
- readykøen er den køen med prosesser som venter på å få tilgang til cpuen.
- hvis et program vil ha cpu så bestemmer denne om den skal utsette andre eller legge den til i readykø

#### medium-term/memory scheduler
- vurderer om prosesser midlertidig skal tas ut av kø
- kjører litt litt oftere enn longterm

#### short-term/CPU scheduler
- laveste nivå av scheduler
- kjører oftest
- bestemmer hvilke prosesser som til hvilken som helst til får lov til å komme inn på prosessen.
- behandler fra readykøen til cpu.

#### klokkeinterrupt
- om man skal kaste ut en prosess fra cpu på klokkeinterrupt eller ikke
- preemptive eller ikke.
- preemptive = operativsystemet kaster ut prosesser slik den føler for d
- nonpreemptive = prosessene kjører til de er ferdige

#### turnaroundtid
- snitt tid hver prosess har på cpuen.
- egentlig: tiden det tar før et program spør på cpu og før den er ferdig i cpu.

#### FCFS/FIFO
- first come first served = fifo = first in first out
- den første som kommer inn får tilgang til cpuen først.
- kjører i cpuen helt til den er ferdig.
- spesielt fordelaktig for lange cpu-bound prosesser.

#### SJF/SPN
- shortest job first = shortest process next
- nonpreemtive - prosessene må gå ut selv.
- store prosesser slipper aldri til prosessen (starvation)
- gitt at man vet hvor lang tid hver prosess faktisk kommer til å bruke.
- __snitt turnaround = summerer (finishtime - arrivaltime) / N__

#### SRTN
- shortest remaining time next
- antar også at man vet hvor mye tid hver prosess kommer til å bruke.
- preemptive
- den prosessen som har minst gjenværende cputid får lov til å kjøre på cpuen
- samme med SJF/SPN at den er fordelaktig for korte prosesser.
- tar vurderingen hver gang en prosess avsluttes eller blir lagt til i readykøen.

#### Round Robin
- preemptive versjon av first come first served (FCFS).
- kaster ut prosessen av cpu etter en tidsperiode (tidskvantum).
  - blir deretter lagt bakerst i FCFS listen.

#### tidskvantum
- tidslengden før man skal kaste ut en prosess fra cpu med round robin scheduling (RR)

#### jiffie
- definisjon av en mengde tid
- ofte definert som tiden mellom to klokkepulser.

#### dynamisk prioritet
- scheduling hvor man har prioritet for hver prosess
- hvor den med høyest prioritet får kjøre først.
- dynamisk med at en prosess kan plutselig få en annen prioritet.

#### aging
- i SJF/SRTN må man vite sånn ca hvor mang tid hver prosess kommer til å bruke
- så dette er en regne teknikk for å estimere hvor lang tid hver prosess kommer til å bruke.
- bruker de siste N turnaround tidene. antar at den nyeste målingen estimerer mest.

#### fair-share/garantert/lottery scheduling
- alle får like mye mulighet til å komme inn til cpu.
  - alle får en lotteri bilett.
  - 1/N sannsynlighet for alle.
- kan gi flere biletter til en høyere prioritetsprosesser.

#### (eget notat) realitet scheduling
- i realiteten så får prosesser prioritet etter om de bruker mer eller mindre av den tiden de får tildelt av cpuen.

#### CPU-pinning/affinity
- man kan si at en prosess kun skal kjøre på en spesifikk cpukjerne
- kan gjøres i HTOP på linux.

#### gang-/co-scheduling
- jobber som bruker mye av de samme dataene blir lagt opp til å kunne kjøre samtidig.
  - derimot på forskjellige kjerner.
- ofte brukt med tråder

# 7 Virtuelt minne, paging og segmentering

#### bitmap
- et bitkart som viser hvilken plass i ram som er ledig og ikke
- hver entry er 4kb.

#### lenka liste
- samme konsept som bitmap bare at det er representert som en lenket liste.
- hvor man har en entry for opptatt og for hull.

#### page
- en side i det virtuelle minnet.

#### page frame
- tilsvarende som page bare i det fysiske ram minnet.

#### offset
- den siste delen av den virtuelle adress delen som sier hvor i den 4kb blocken dataen refererer til.
- den delen av adressen som ikke går igjennom virtuelle minnet
  - men blir kopiert direkte over til den fysiske adressen.

#### virtuelle/fysiske adresser
- virtuelle adresser er den adressen som blir sendt fra cpu til MMU
- blir oversatt av MMU med hjelp av page table til den fysiske adressen.
  - som peker til direkte til ram.

#### virtuelt/fysisk adresserom
- det virtuelle adresserom finnes ikke. det er bare en abstraksjon.
  - det er det adresserommet som prosessene forholder seg til
  - er 2^n stort. hvor n er antall bits i den virtuelle adressen som brukes i page table

#### page table
- tabellen som inneholder alle page entries

#### page fault
- den virtuelle adressen finnes ikke i pagetable.
  - genererer et interrupt, exception
- må derfor enten hentes fra memory eller fra disk.
  - avhenger om det er soft eller hard miss.

#### paging
- det å holde selve page table oppdatert kalles paging.

#### page table entry
- en side i page table
- inneholder:
  - page frame number
    - tallet som page entrien skal oversettes til
  - caching disabled
  - present/absent
  - protection
  - referenced
  - modified

#### present/absent bit
- om gjeldene table entry er gyldig eller ikke.
- om den finnes i memory.

#### referenced bit
- om table entry har blitt brukt eller ikke
- blir nullstilt hver klokkeinterrupt.

#### modified/dirty bit
- om gjeldene table entry har blitt skrevet til.
- og må dermed skrives til disk.
- se write through og write back.

#### TLB
- Translation lookaside buffer
- en buffer med nylge oversatte virtuelle adresser.
- går til page table hvis den ikke er i tbl.
  - ved en TLB miss.

#### TLB miss
- man finner ikke page nummeret i TLB.

#### major/minor page fault
- minor
  - fant ikke page entry i TLB eller

#### multilevel page table
- deler opp i flere lags tabeller
- der den øvre tabellen gir pageframe til en ny tabell under seg.

#### invertert page table
- en page table som er motsatt vridd
- fra: page number ==> page frame
- til: page frame ==> page number
- lite brukt i praksis
- bruker søketeknikker for å finne rett table entry
  - søker etter rett pagenumber.

#### segmentering
- no longer supported.

## kap 7 Bash:
- alle tekstfil/input relaterte kommandoer kan brukes med '|'

#### /$()
- sett utførelse av kommando inn i variabel

#### $PATH
- hvor linux leter etter executable filer

#### ls
- lister alle directories

#### pwd
- mappa man er i nå

#### ln
- create shortcuts (symbolic links)

#### touch
- change file timestamps

#### stat
- get filestatus

#### sort
- sorter innholdet i en tekstfil linje for linje.

#### uniq
- skriver ut innholdet i en fil men hver linje er unik

#### tr
- oversett innhold i tekstfil. feks fra upper til lowercase
- whitespace til tabs

#### cut
- skriv ut bare deler av en textfil

#### rev
- reverser en tekstfil

#### more/less
- less
  - skriv ut innholdet i en tekstfil en side av gangen
- more
  - samme som less men kan ikke gå bakover.

#### head
- skriv ut de første N linjene/bytes

#### tail
- skriv ut de siste N linjene/bytes.

#### xargs
- simuler standard input inn til en commando
- echo "newdir" | xargs mkdir
  - lager ny folder som heter newdir

#### sed
- stream editor
- kan gjøre mye funksjonalitet på en tekstfil.
  - søking, find/replace

#### fg
- legge en bakgrunnsjobb i forgrunnen.

#### bg
- legge en forgrunnsjobb i bakgrunnen.

#### date
- skrive ut nåværende dato og tid
- endre nåværende dato og tid

#### uname
- skriv ut informasjon om systemet

#### du
- disk usage
- estimerer fil størrelse

#### df
- estimerer hvor mye fil som er brukt for et gitt filsystem

#### which
- lokaliser executable filene assosiert med en gitt kommando

#### tee
- leser standard input
- skriver det ut samtidig som den skriver det til en fil.

#### mktemp
- lage temporary filer eller mapper
  - med en gitt størrelse.
  - i /tmp
- også skriver den ut navnet til den.

#### pwgen
- må installeres
- password generator

#### CTRL-R/Z/C/D/L
- R
  - søke etter en kommando
- Z/C
  - avslutte en kjørende job
- D
  - send EOF til fil.

#### &
- på slutten av en kommando
- kjører da i bakgrunnen

#### &&
- kjøre to kommandoer etter hverandre
- den andre kjører hvis den første var vellykket.

#### $?
- hent exit kode av den sist kjørte kommandoen
- 0 vellykket 1 ikke vellykket

#### $1
- attributt 1 når kommandoen ble kjørt.

#### >
- skriv output til fil

#### >>
- skriv output til fil men appende.

#### 2>&1
- $ cat nop.txt > output.txt 2>&1
- $ cat output.txt
    - cat: nop.txt: No such file or directory
- regner med error som vanlig stdout

#### ?
- ukjent wildcard

# kap 8

#### second-chance
- samme som fifo liste.
- prøver å kaste den som er lengst siden har blitt lagt til.
- hvis den har referenced = 0 så blir den kastet ut
- hvis den ikke har det blir den lagt fremst i lista.
  - som om den var helt ny.

#### NRU
- Not recently used
- leser av referenced og modified
- deler de opp i ulike klasser etter vikighet og sletter en tilfeldig side fra laveste klasse
  - class 0 : 0 referenced 0 modified. not ref not mod.
  - class 1 : 0 referenced 1 modified
  - class 2 : 1 referenced 0 modified
  - class 3 : 1 referenced 1 modified

#### LRU
- Least recently used
- anta at den pagen som er lengst siden er brukt kommer minst sannsynlig ikke til å bli brukt i nærmeste fremtid.
- pleier å ha en teller som teller opp. og letter tiden til pagen når den blir akksessert

#### demand paging
- pager allokeres når de blir spurt på.

#### prepaging
- pager fram working set før programmet laster inn.

#### working set
- sett med pageentries som en prosess trenger

#### thrashing
- når et program prøver å bruke virtuellt minne når det ikke er mer plass
- generer masse page faults

#### RSS
- resident set size
- working set bare på linux

#### lokal/global page replacement
- om man skal kjøre en page replacement algoritme
- lokalt
  - fjerner kunn fra sine egne prosesser
- globalt
  - kan fjerne fra alle prosseser.

#### page sharing
- det faktum at flere applikasjoner deler mye informasjon.
- derfor setter man ofte bibloteker i ram og opprettholder pointere til disse.

#### memory mapped filer
- mapper en referanse i det virtuelle minnet til en fil eller enhet.
- slik at X bytes kan hentes

#### copy-on-write
- ved forks så lages en eksakt kopi av en prosess sin working set.
  - skjer når forken prøver å modifisere workingset page som er read only.
- denne kan forken jobbe så mye den vil på

# kap 9

#### symbolsk/hard link
- hard links linker til en spesiell plass på harddisken.
  - en directory entry, peker til en inode
- fil som bare referer til en annen fil. inneholder bare path

#### character/block device
- character device, sender ascii verdier serielt. tastatur/mus osv
- block device, harddisk

#### attributter/metadata
- data som forteller noe om filen. men ikke hovedinnholdet.

#### filrettigheter
- hvem som har tilgang til en gitt fil.
- ofte brukere.
- eksiterer ikke med FAT

#### fopen/fclose
- åpne og lukke filer systemkall

#### fildeskriptor
- abstraksjon for input/output ressurs
- feks stdin stdout stderr

#### STDIN/STDOUT/STDERR
- stdin = input
- stdout = output
- stderr = error

#### cwd
- change working directory

#### partisjon
- største oppdelings abstraksjonen av en harddisk
- Blokkgrupper x Blokker pr gruppe x Blokkstørrelse

#### datablokk
- minste del av oppdelingsabstraksjon
- 1 fil har en datablokk

#### blokkstørrelse
- finnes ulik størrelse på hver blokkstørrelse
- kan ikke være for stort, hvis en fil er mindre enn størrelsen sløses plass

#### i-node
- datastruktur til å holde informasjon om en fil eller mappe
- peker på datablokkenes plassering til den gitte filen.

#### timestamps
- når sist fil ble åpnet/endret
- forteller noe om tid.

#### indirekte adressering
- adressering i en inode skjer til en block full av adresser til datablokker
- kan ha single, double og trippel indirekte blokker
- med økende grad av kapasitet til blokkadresser.
- en double peker til en single, trippel til double til singel.
- brukes når det er mer en 12 diskadreser i inoden.

#### linkteller
- hver fil har en teller som sier hvor mange hardlinks som eksisterer til den gitte filen

#### LFS
- log structured file system
- loggfører alt som gjøres av skrivinger i en egen memory segment. skriver den etterhvert til disk.
- skriver bare sekvensielt. skriver aldri over data.

#### journalling
- logfører alt som operativsystemet gjør før den gjør det
- gjør at man kan rette opp feil senere. feks ved et strømbrudd midt i en diskskriving.

#### idempotent
- en operasjon som logføres i journalling systemet må være idempotent
- betyr at operasjonen kan gjøres mange ganger uten å skade systemet.
- "until the cows come home with no danger"

#### VFS
- virtual file system
- operativsystemer har ofte filsystem tolkere for alle filsystemer.
- disse tolkerne kaller VFS interfaces
- oversetter viruelle

# kap 10/11

#### isolert/memory-mapped IO
- ved isolert så er portene til io enhetene i sitt eget adresserom
- ved memory mapped er de i samme adresserom som memory
- finnes også en hybrid som intel bruker

#### IO-porter
- 16 bits tall
- forteller hvordan cpu skal snakke med gitt io enhet.

#### presise/upresise interrupt
- upresise interrupts er interrupt som ikke oppfyller kravene for presise.
- upresise interrupts er veldig krevende for OS. må finne ut hva som skjedde
  - og hva som skal skje.
- ved presise interrupts så oppfylles disse kravene
  - Pc lagres på et kjent sted
  - alle instruksjoner før pc er ferdig
  - ingen instruksjoner etter pc er kjørt
  - kjent tilstand til pc

#### programmert/interrupt-basert IO
- gjøre input og output med fullmakt (programmert)
  - cpu gjør all jobben.
- eller bruke interrupts

#### device driver
- hver enhet har i grunn hver sin driver.
- som snakker mellom cpu/dma
- "device-specific code for controlling it"

#### seek time
- tiden det tar å komme fra A til B på en harddisk

#### transfer rate
- hvor mye data som kommer inn fra en harddisk per sekun
- gb/s mb/s osv.

#### virtuell geometry
- blokker er ikke delt jevnt utover en disk
- men er inndelt slik at hver sektor er like stor. dette kalles soner.
- denne geometrien representeres fra kontrolleren

#### LBA
- logical block adressing.
- adresserer fra 0te sektor og opp til så mange sektorer som eksisterer.
- eksisterte en grense for hvor mange sektorer på ibm pcer før i tiden.

#### NAND flash
- mange pittesmå not and gater. brukes som lagringsmedium for ssd

#### SLC/MLC/TLC
- hvor mye data som kan lagres på hvert celle nivå
- slc kan lagre 0 og 1
- mlc kan lagre 4 ulike tall, 11, 10, 01, 00
- mlc har lavere skrive kapasitet pga utsmørningen som skjer mellom oppdelingene hver skriving.

#### trim
- kommando som forteller SSD om en gitt fil skal wipes eller ikke.
- viktig med tanke på at ssd sletter forskjellig fra vanlig hdd
- ved sletting må man lese inn hele sektoren og skrive den til en ny plass.
- hver sektor på ssd tåler kunn x antall writes. derfor bedre å skrive til ny.

#### write amplification
- når man skriver x mengde data til en ssd, så blir det egentlig skrevet mer.
- dette er fordi man skriver også data som lå i samme sektor.
- man må flytte mye rundt på data for å få ting til å fungere
- derfor skriver man egt mer enn det man får som input.

#### wear levelling
- teknikker for å forlenge holdbarheten til flash memory

#### defragmentering
- data ligger ikke etter hverandre på en hdd
- derfor blir det mye fram og tilbake når man skal aksessere.
  - defragmentering er en prosess som fikser på dette
  - legger data relatert til hverandre etter hverandre
  - slik at sekvensiell lesing kan skje
- noe man ikke trenger på en ssd.

#### RAID
- teknikker for å fordele data utover flere disker
- Raid 0
  - skriver data bortover diskene
- Raid 1
  - lager et speilbilde av en disk
- Raid 5
  - skriver vanlig som ved raid 0
  - men har en paritetsbit innimellom
    - jevner ut antall bits samlet
    - brukes til å kunne recovre data.

#### intern/ekstern fragmentering
- intern
  - et program fåt tildelt mer memory enn det den trenger
  - får dermed memory som ikke kan brukes. og bare tar plass.
- ekstern
  - man får en blokk med ledig plass mellom to programmer
  - må dermed flytte alt opp et hakk.
- avhenger av perspektiv

#### datarate
- hvor mye data som går per sekund

#### plassutnyttelse
- hvor mye plsas som faktisk brukes?
- det er lite fragmentering på gang

#### blokkadresser
- en adresse som peker på en blokk
- eller egentlig en fil da.

#### filsystemkonsistens
- at bitmaps struktur stemmer overens med det som faktisk skjer i en ssd
- ledig er faktisk ledig osv

#### buffer/page cache
- cacher de mest brukte blokkene på hdd etter LRU

#### cluster
- på fat filsystemet er blokker kalt clustre.

#### MFT record
- master file table record
- NTFS!
- en record representerer en fil eller mappe

#### resident/immediate fil
- om en gitt fil får plass i MFT record. med sin datastrøm.

#### diskblock runs
- indikerer hvilke blocker som sekvensielt ligger ved siden av hverandre.
- fil a ligger mellom 20-23 run 1
- fil a ligger også mellom 64-65 run 2

#### iops
- input output operations per second
- sier noe om hvor god en io enhet er. men ikke nødvendigvis real world applikerbar.

#### sequential/random read/write
- sequential
  - leser/skriver fra A til B. serielt
- random
  - leser/skriver tilfeldig plass

# 12

#### preemptable/nonpreemptable ressurser
- preemptable
  - ressurs som kan avbrytes midt i en operasjon
- non preemptable ressurs
  - ressurs som må bli ferdig før den kan gjøre noe annet.
  - feks brenning av en cd.

#### mutual exclusion
- generelt betyr dette at ulike enheter unngår at ting skjer samtidig.
- 2 prosesser kan ikke ha samme ressurs samtidig.
- en ressurs er enten assignet til 1 prosess eller ledig.

#### circular wait
- flere prosesser venter på hverandre i en stor sirkel.

#### ressursgraf
- fremstillelse av prosesser og ressurser.
- hvor hver ressurs kan ha en prosess og ønske den.
- når den er ønsket gjør den busy waiting, til den er ledig.
- bruker mutexes for tilgangskontroll til hver enkelt ressurs.

#### Bankers
- optimal algoritme for ressurs scheduling.
- referer til en bank hvor man deler ut kreditt til hver prosess.
- velger den prosessen som mangler minst kreditt til å oppnå maksimal ressurs behov.
- derimot umulig algoritme i praktis.
  - pga vi ikke vet maksimal ressurs behov.

#### safe/unsafe tilstand
- safe tilstand
  - vi kjøre prosessene med en forsikring om at deadlock ikke kommer til å skje
- unsafe tilstand
  - vi kjører prosessene men deadlock kan skje.

#### claim/allocation/max/request matrix/vector
- bankers algoritme representert som matriser
- claim matrix
  - hver prosess sitt maksimal behov per ressurs.
- allocation matrix
  - hvor mye hver prosess har nå av hver ressurs.
- request matrix (C-A)
  - hvor mye hver prosess mangler for å oppnå claim matrix

# 13
#### unikernel
- man kan samle inn det applikasjon man trenger for å kjøre sitt program
- dette kombineres da til et operativsystem som man da kan kjøre

#### sensitive/privilegerte instruksjoner
- priviligerte instruksjoner er instruksjoner som skaper et trap.
  - aka et bytte fra user til kernel mode
- sensitive. de som prøver å endre systemet på en eller annen måte

#### trap-and-emulate
- tar imot instruksjoner fra guest os
- fanger dette og emulerer det som trengs å emuleres

#### binæroversetting
- overseter assembly kall til kall til hypervisor

#### basic blocks
- blokker som egentlig skaper en branch i assembly.
- aka jump, if osv
- dette oversettes da til kall i hypervisor

#### paravirtualisering
- er et interface som blir presentert for alle de virtuelle datamaskinene
- for eksempel vmm
- dette fungerer med hypercalls

#### hardwarestøttet virtualisering
- hvor man har hardware som er spesielt laget for å kunne drive med full-virtualisering.

#### vmx/svm/ept/npt/vpid/asid/vt-d
- ulike former for hardware støttet virtualisering
- vmx
  - intel virtualisering
- svm
  - amd virtualisering
- ept
  - Intel extended page table support enabled to make emulation of guest page tables faster.
- npt
  - amd nested page tables
- vpid
  - intel virtual processor ID

#### shadow/guest/physical page table
- shadow table
  - page table som mapper fra det virtuelle adresserommet til host os fysisk
- guest table - hos guest os
  - page table som mapper fra det virtuelle adresserommet til det fysisket
- physical table - hos host os
  - page table som mapper fra det fysiske rommet hos guest os til det fysiske
  - hos host os

#### page walk
- den jobben MMU gjør når den skjekker om den har en oversettning tilgjengelig.
- går igjennom hele tlb og ser om en gitt oversettelse eksisterer.

#### CR3
- register nummer 3, i x86
- holder adressen til øverste page table. brukes når en page walk gjøres senere.

#### ballooning
- trekker til seg adresseringer som en ballong.
- dette gjøres slik at host operativsystemet kan få mer ram.

#### IOMMU
- io memory management unit
- adresserer akkurat på samme måte som de komemr fra cpu.
- har tlb.
- har også en device table som tilordner io enheter direkte til en gitt vm

#### SR-IOV
- intels fantastiske maskin som har et nettverkskort dedikert for virtuelle maskiner.
- hypervisor trenger ikke å blande seg inn i nettverksdelingen.
- single root I/O virtualisation.
- manipulerer kommunikasjonen mellom driveren og enheten.
- gjøres for å optimalisere virtualisering.

#### virtual function
- presenterer en virtuell funksjon til gjeste oset.
- som er helt likt det fysiske, men får gjeste oset til å tro at den sjeleeier av enheten.
- det er akkurat dette som blir gjort på SRIOV

#### nested virtualisering
- kunne kjøre en virtuell maskin innen en annen virtuell maskin
- nyttig når man skal teste programvare som krever flere hostsystemer.

#### cgroup
- limiterer ressursbruk for en gruppe med prosesser.

#### namespaces
- kan introdusere spesielle greier til hver enkelt container.
- eks administrere mount points til hver enkelt container.
- refererer til spesifikke ressurser.
- partitsjonerer kernel ressurser.

#### union mounts
- kombinerer flere mapper til en mappe som ser ut som en enkelt mappe.

#### docker
- kontainer kjører.


# 14

#### CIA
- total målene når det gjelder beskyttelse for elementer
- dette må opprettholdes for å ha god sikkerhet
- Confidentiality
  - noe skal ikke være tilgjengelig for noen andre
- Integrity
  - informasjonen som vises er rett
- Availability
  - informasjonen er tilgjengelig

#### adversary
#### symmetrisk/public-key krypto
- symmetrisk bruer samme nøkkel til å kryptere og dekryptere
- public-key krypto aka asymmetrisk
  - 2 nøkkler brukes
  - en public nøkkel til å kryptere
  - en privat nøkkel for å dekryptere

#### hash-funksjon
- skaper en spesiell uleselig output for hver eneste unike fil.
- spesielle mattematiske formler
- sha512, md5

#### sertifikat
- bruker public key kryptering til å verifisere faktisk identitet.
- mye brukt for nettsider.

#### PKI
- Public Key infrastructure
- selve infrastrukturen som bruker sertifikater og Public key

#### security by obscurity
- sikkerhet med ukjenthet.
- taktikken med å prøve å holde kryptering og dekrypteringsalgoritmer hemmelig.
- fungerer nesten aldri, brukes av sikkerhetsamatører.

#### TPM
- Trusted Platform modules
- en modul på en gitt enhet som er __Trused__
- denne modulen utfører kryptering/dekryptering og oppbevarer nøkkler
- finnes alltid i kernel space.

#### beskyttelses-domene/matrise
- beskyttelses domene
  - det en gitt bruker har tilgang til.
  - trenger ikke bare å være filer. kan også være i/o enheter.
- matrise (Protection matrix)
  - en matrise som viser hva de ulike brukerne(domenene) har tilgang til
  - domene nedover, og filer bortover

#### ACL
- Access control list
- en liste som definerer beskyttelses matrise
- for hver fil finnes en liste over hvilke rettigheter hvert domene har.
- __skjekkes i REKKEFØLGE__
- tar da den første entrien som har rett domene.
- settes aldri opp manuelt.

#### capability list
- lik tankegang som i ACL
- her er det en liste over hva hvert domene har tilgang til.
- ACL som er mest brukt

#### UID
- User id
- linux

#### GID
- group id
- linux

#### SID
- Security id (Windows)
- hver bruker og gruppe har hver sin unike id på verdensbasis.

#### reference monitor
- monitor mellom to elementer som snakker med hverandre med kryptering.
- ligger i TPM, mottar systemkall fra userspace.

#### trusted computing base
- hvor alt av software og hardware ligger
- som overholder at sikkerhetsregler blir fulgt.

#### multilevel security
- sikkerhetsmekanismer som bygger på å ha ulike nivåer av sensistivitet.
- hvor man skiller hvor "available" noe skal være.

#### DAC/MAC
- DAC - discretionary access control
  - tilgangskontroll hvor brukeren kan endre på hvem som har tilgang til hva.
  - veldig fritt å endre hvem som har tilgang til hva.
  - hjemmepc
- MAC - Mandatory access control
  - hvis man jobber hos militæret, skal det ikke være slik at man kan endre på alt.
  - dette er PÅKREVD access control. hvor man jobber med sikkerhetsrangeringer og slikt.

#### Bell-La Padula
- sikkerhetsmetode slik som blir brukt i militæret. for CONFIDENSIALITY
- hvor man har ulike grader av sensistivitet
- prosesser på et gitt nivå kan bare skrive oppover og lese nedover sin egen rank.
- "a lieutenant can ask a private to reveal all he knows and then copy this information into a general’s file
without violating security."

#### Biba
- for INTEGRITY - consistency
- problematisk hvis et vanlig firma bruker bell-la padula
- kan skrive over viktig informasjon hos sjefen med et uhell.
- man reverserer reglene for bell-la padula.
- kunn skrive nedover og lese ovenfra.
- kunne lese notater fra sjefen men jobbe på filer under seg selv.

#### covert channel
- hemmelig støyete lytte kanal mellom 2 domener
- uønskelig
- må lytte nøye igjennom for å finne noe.

#### access token
- windows bruker access token hos prosesser.
- inneholder masse deskriptiv sikkerhets informasjon for et objekt
- blant annet usersid
- og integrity level.

#### security descriptor
- liste hos objektet.
- inneholder DACL og SACL
- DACl er en liste over masse ACLer
- SACl er en oversikt over hva som skal loggføres for objektet.

#### integritetsnivå
- er i access token
- siden windows er en blanding av MAC og DAC. så finnes det et nivå for integritet
- høy integritet - kan endre på mye, som er over seg selv.
- lav integritet - kan endre på lite. bare på det som er under seg selv.

#### windows privileges
- hvilke spesielle systemkall en gitt prosess kan gjøre
- kunne gi bort deler av det en administrator kan gjøre til en prosess.


#### MIC
- mandatory integrity control
- skjekker at integritetsnivået stemmer overens med nivået som ligger i DACL

#### UAC
- user account control
- tilatter at en prosess kan ha visse administrator premissions uten å ødelegge systemet.
- prosessen tror den kjører som admin men egentlig bare en abstraksjon.

#### SetUID/SetGID
- linux har en 4bits strøm som representerer en fils tilgangrettigheter for hvert domene.
- som oftest RWX for bruker-grupper-andre
- men kan også ha SetUID, SetGid eller stickybit.
- setuid
  - kunne kjøre et program som om det var eieren av programmet som kjørte den.
  - aka kunne kjøre et program som root.
  - eks passwd som må endre data i en password folder
- setgid
  - samme som ovenfor bare for en gruppe.

  | tall | type |
  | ---- | ---- |
  |1| x - stickybit |
  |2| w - SetGid|
  |4| R - setGID|

- setuid/gid er eget tall før sin gruppe
   - representert som s-s-t

#### sticky bit
- brukes ikke nå lengre
- betydde før at "hold denne proseessen i minne hvis du kan"

#### linux capabilities
- man deler opp ting som superuser pleier å gjøre i ulike capabilities.\
- dette kan da bli tildelt ulike threads.

#### apparmor/selinux
- selinux
  - security enhanced linux
  - linux som er ekstra sikker
- apparmor
  - linux som lar admin kunne gi premissions til hvert enkelt program.
# 15
#### buffer overflow
- et program leser inn flere tegn enn det den egentlig er assignet for.
- da kan man skrive inn mer data i memory enn det som ble tildelt.
- dermed kan man skrive over retur adresse området. som egentlig peker tilbake til main.
- kan da fåes til å peke på et område som inneholder skadelig kode.

#### heap spraying
- det er ikke alltid man vet ca hvor returadressen ligger.
- dette kan da løses med heapspraying.
- man sprayer da heapen med NOP. og har en pekeadresse på slutten.
- som da peker på skadelig kode.

#### stack canary
- legger inn en verdi før returadressen.
- skjekker da om den koden er endret
- hvis den er endret er det da noe feil.

#### function pointers
- det finnes som sagt mange ulike pekere man kan tulle rundt med.
- funksjonspekere er en av dem. brukes i pthread.
- peker som peker til en funksjon og ikke en verdi.
- poenget er at alle function pointers nåelig fra en buffer overflow er utsatt.

#### DEP/NX
- DEP
  - forsvar!
  - Data exectuion protection
  - memory skal aldri være skrivbar og eksekurbar samtidig.
- NX
  - non execute bit
  - bit som brukes til å skille mellom eksekverbar og lesbar memory

#### return to libc
- libc, biblotek all c programmer bruker
- brukes til å gjøre systemkall til kernel.
- poenget er at man kan manipulere et program til å utføre et gitt systemkall
- ut ifra hvor i minnet ulike systemkall ligger.
- kan da også gi med parametere.

#### ROP
- Return oriented programming
- bruker data som allerede memory
- kobler disse sammen til å lage et system som kan er skadelig.

#### ASLR
- Adress space layout randomization
- tilfeldiggjør hvor ulike function pointers blir lagt i memory
- forhindrer return to libc og andre angrep.
- randomiserer også plasseringen på heap, stack og biblotekene.

#### format string attack
- printf er sårbar.
- printf("%" buffer) er sikker men ikke printf(buffer)
  - på den første må buffer være string
  - på den siste kan buffer være hva som helst.
  - printf har funksjonalitet til å skrive tilbake til variablene som er brukt i kommandolinjen.

#### dangling pointer
- error som blir skapt i C
- 1. assigner memory til program med malloc
- 2. frigjøres med free()
- 3. bruker memory som nylig blir frigjort
- 4. skriver over memory som nå ligger i det dataområdet i memory.

#### integer overflow
- bruker mer integer memory enn det som var assignet
- som da er en sårbarhet..

#### command injection
- samme greie som med sql injection
- får et program til å kjøre kode uten den har vært klar over det.
- fort kan dette være fork()

#### spoofing
- lage noe som ser ekte ut.
- får da brukeren til å skrive inn sårbar informasjon.

#### trojan
- program som later som om den er noe annet enn det den er.
- viser seg egentlig å være skadelig programvare

#### virus
- programvare som er skadelig og kopiererer seg selv.

#### polymorfisk
- programvare som som er skadelig.
- endrer seg for hver gang den kopierer seg selv.
- masse ubrukelig kode som nop og add 0 blir lakt til.
- for å villede antivirus programvare.

#### orm
- virus men bare over internettet

#### spyware
- programvare som sender personlig informasjon over nettet.
- blitt noe endret etter inføring av moderne telefoner.

#### rootkit
- program vare som prøver å skjule seg selv over lengre periodisk tid.
- selve programmet kan være en av de typene nevnt tidligere.
- ulike typer  å gjemme seg på.
- firmware
  - rootkits som legger seg i firmware. ofte i bios.
  - vanskelig oppdage. spesielt hvis de en/de krypterer seg selv før og etter bruk.
- hypervisor
  - installerer seg selv i hypervisor.
  - får det vanlige operativsystemet til å kjøre som en guest os.
  - oppleves som et vanlig os, men er egentlig en vm i kontroll av inntrengeren.
- kernel
  - rootkit som legger seg i en kernel modul eller en enhetsdriver.
- libary
  - legger seg i et biblotek feks libc,
- application
  - legger seg i et program.
  - ofte et program som skriver/leser mange filer
  - absurd nokk at ingen skjekker der

#### code signing
- kjør bare kode som er signert fra leverandører som er til å stole på
- og som er uendret.
- bruker digital programvare signering.
- bruker public key kryptering.

#### jailing
- en kontrollør går igjennom all kode før den blir eksekvert.
- for å skjekke om den ikke inneholder skadelig programvare

#### sandboxing
- hvert program har et DEFINITIVT område den kan arbeide på i virtuelt omeråde.
- kan da ikke referere eller arbeide på data som ligger utenfor sin sandbox.
-
