#### CACHE
https://manybutfinite.com/post/intel-cpu-caches/
- Each page has 4KB / 64 bytes == 64 cache lines in it.
- In a fully associative cache any line in memory can be stored in any of the cache cells.  a fully associative cache is not a good trade off in most scenarios.
- this cache is set associative, which means that a given line in memory can only be stored in one specific set (or row) shown above. So the first line of any physical page (bytes 0-63 within a page) must be stored in row 0, the second line in row 1, etc. Each row has 8 cells available to store the cache lines it is associated with, making this an 8-way associative set.
- Each cached line is tagged by its corresponding directory cell; the tag is simply the number for the page where the line came from.
